package funix.prm.prm391x_shopmovies_makaarimahnrFX07086;

import android.view.View;

public interface OnRecyclerItemClick {
    public void recyclerViewListClicked(View v, int position);
}
