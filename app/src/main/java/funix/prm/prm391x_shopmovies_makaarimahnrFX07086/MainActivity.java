package funix.prm.prm391x_shopmovies_makaarimahnrFX07086;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.google.android.material.tabs.TabLayout;
import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private List<Fragment> fragments = new ArrayList<>();
    private ViewPager vp;
    private TabLayout tabLayout;
    private final String TAG = "MAIN ACTIVITY";


    @Override
    protected void onStart() {
        super.onStart();
        if(FirebaseAuth.getInstance().getCurrentUser() == null){
            startActivity(new Intent(MainActivity.this, LoginActivity.class));
            finish();
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fragments.add(new HomeFragment());
        fragments.add(new ProfileFragment());

        tabLayout = findViewById(R.id.tab_layout);
        vp = findViewById(R.id.main_vp);

        PageAdapter pageAdapter = new PageAdapter(getSupportFragmentManager(),
                FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT,
                fragments);
        vp.setAdapter(pageAdapter);

        tabLayout.setupWithViewPager(vp);

        try{
            tabLayout.getTabAt(0).setIcon(R.drawable.ic_baseline_storefront_24);
            tabLayout.getTabAt(1).setIcon(R.drawable.ic_baseline_person_24);
        }catch(Exception e){
            Log.e(TAG, e.getLocalizedMessage());
        }
    }
}