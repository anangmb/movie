package funix.prm.prm391x_shopmovies_makaarimahnrFX07086;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;

public class InfoActivity extends AppCompatActivity {
    ImageView back,poster;
    TextView title, price;
    private final String TAG = "INFO";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);
        back = findViewById(R.id.info_back);
        poster = findViewById(R.id.info_poster);
        title = findViewById(R.id.info_title);
        price = findViewById(R.id.info_price);

        String extra = getIntent().getStringExtra("movie");
        if(extra!=null){
            try {
                Movie movie = new Gson().fromJson(extra,Movie.class);
                if(movie != null){
                    if(!TextUtils.equals(movie.getImage(),null)){
                        Glide.with(InfoActivity.this)
                                .load(movie.getImage())
                                .apply(new RequestOptions().centerCrop())
                                .into(poster);
                    }

                    if(!TextUtils.equals(movie.getTitle(),null)){
                        title.setText(movie.getTitle());
                    }
                    if(!TextUtils.equals(movie.getPrice(),null)){
                        price.setText(movie.getPrice());
                    }
                }
            }catch (Exception e){
                Log.e(TAG, e.getLocalizedMessage());
                Toast.makeText(InfoActivity.this, e.getLocalizedMessage(),Toast.LENGTH_SHORT)
                        .show();
            }
        }

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}