package funix.prm.prm391x_shopmovies_makaarimahnrFX07086;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Client {
    private static Retrofit retrofit = null;

    /**
     * get retrofit client
     * @return retrofit client ready to use
     */
    public static Retrofit getClient(){
        retrofit = new Retrofit.Builder().baseUrl("https://api.androidhive.info/json/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit;
    }
}
