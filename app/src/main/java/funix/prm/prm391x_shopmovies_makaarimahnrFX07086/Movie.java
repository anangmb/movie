package funix.prm.prm391x_shopmovies_makaarimahnrFX07086;

public class Movie {
    private String title, price, image;

    /**
     * Create new instance of Movie
     * @param title The title of the movie
     * @param price The price of the movie
     * @param image The image of the movie
     */
    public Movie(String title, String price, String image) {
        this.title = title;
        this.price = price;
        this.image = image;
    }

    /**
     * To get the title of the movie
     * may thrown null pointer exception
     * @return the movie's title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Set the current movie title
     * @param title New movie title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * To get the title of the movie
     * may thrown null pointer exception
     * @return Movie's price
     */
    public String getPrice() {
        return price;
    }

    /**
     * Set new price for current movie
     * @param price new movie's price
     */
    public void setPrice(String price) {
        this.price = price;
    }

    /**
     * Get movie's image url
     * @return movie's image (poster) url
     */
    public String getImage() {
        return image;
    }

    /**
     * Set new movie image
     * @param image new movie image url
     */
    public void setImage(String image) {
        this.image = image;
    }
}
