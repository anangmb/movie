package funix.prm.prm391x_shopmovies_makaarimahnrFX07086;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface MovieInterface {
    @GET("movies_2017.json")
    Call<ArrayList<Movie>> getMovies();
}
