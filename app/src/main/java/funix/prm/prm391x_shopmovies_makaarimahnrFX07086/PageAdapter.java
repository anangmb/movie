package funix.prm.prm391x_shopmovies_makaarimahnrFX07086;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.List;

public class PageAdapter extends FragmentPagerAdapter {
    List<Fragment> fragments;

    /**
     * Create instance of Page Adapter fro view pager
     * @param fm activity's fragment manager
     * @param behavior behavior of the FragmentPagerAdapter
     * @param fragments list of fragment used to navigate
     */
    public PageAdapter(@NonNull FragmentManager fm, int behavior, List<Fragment> fragments) {
        super(fm, behavior);
        this.fragments = fragments;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        if(position == 1){
            return "Profile";
        }else{
            return "Home";
        }
    }
}
