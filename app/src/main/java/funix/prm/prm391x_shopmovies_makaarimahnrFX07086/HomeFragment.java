package funix.prm.prm391x_shopmovies_makaarimahnrFX07086;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class HomeFragment extends Fragment implements  OnRecyclerItemClick{
    RecyclerView rv;
    ArrayList<Movie> movies = new ArrayList<>();
    private final String TAG = "HOME";


    public HomeFragment() {
        // Required empty public constructor
    }

    public static HomeFragment newInstance(String param1, String param2) {
        HomeFragment fragment = new HomeFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_home, container, false);
        rv = view.findViewById(R.id.home_rv);
        GridLayoutManager grm = new GridLayoutManager(getActivity(),2,RecyclerView.VERTICAL,
                false);
        rv.setLayoutManager(grm);
        MovieAdapter adapter = new MovieAdapter(movies, getActivity(),this);
        rv.setAdapter(adapter);


        MovieInterface service = Client.getClient().create(MovieInterface.class);
        Call<ArrayList<Movie>> call = service.getMovies();
        call.enqueue(new Callback<ArrayList<Movie>>() {
            @Override
            public void onResponse(Call<ArrayList<Movie>> call, Response<ArrayList<Movie>> response) {
                if(response.code() == 200){
                    movies.addAll(response.body());
                    adapter.notifyDataSetChanged();
                }else{
                    Toast.makeText(getActivity(),"Error " + response.code(),
                            Toast.LENGTH_SHORT)
                            .show();
                    Log.e(TAG,"Error " + response.code());
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Movie>> call, Throwable t) {
                Toast.makeText(getActivity(),t.getLocalizedMessage(),
                        Toast.LENGTH_SHORT)
                        .show();
                Log.e(TAG,t.getLocalizedMessage());
            }
        });


        return view;
    }

    @Override
    public void recyclerViewListClicked(View v, int position) {
        Intent i = new Intent(getActivity(),InfoActivity.class);
        i.putExtra("movie",new Gson().toJson(movies.get(position)));
        startActivity(i);
    }
}