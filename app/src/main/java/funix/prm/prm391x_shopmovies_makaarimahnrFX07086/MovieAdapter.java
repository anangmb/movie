package funix.prm.prm391x_shopmovies_makaarimahnrFX07086;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.ViewHolder> {
    ArrayList<Movie> movies;
    Context mContext;
    OnRecyclerItemClick itemClick;

    /**
     * Create new recycler view adapter for movie item
     * @param movies List of movie to be shown
     * @param mContext Current activity context
     */
    public MovieAdapter(ArrayList<Movie> movies, Context mContext,OnRecyclerItemClick itemClick) {
        this.movies = movies;
        this.mContext = mContext;
        this.itemClick = itemClick;
    }

    @NonNull
    @Override
    public MovieAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.movie_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MovieAdapter.ViewHolder holder, int position) {
        Movie movie = movies.get(position);
        if(movie != null){
            if(movie.getImage() != null){
                Glide.with(mContext)
                        .load(movie.getImage())
                        .apply(new RequestOptions().centerCrop())
                        .into(holder.poster);
            }
        }
        if(movie.getTitle() != null){
            holder.title.setText(movie.getTitle());
        }
        if(movie.getPrice() != null){
            holder.price.setText(movie.getPrice());
        }
    }

    @Override
    public int getItemCount() {
        return movies.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        ImageView poster;
        TextView title, price;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            poster = itemView.findViewById(R.id.movie_poster);
            title = itemView.findViewById(R.id.movie_title);
            price = itemView.findViewById(R.id.movie_price);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            itemClick.recyclerViewListClicked(view,getLayoutPosition());
        }
    }
}
